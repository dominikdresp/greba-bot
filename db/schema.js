const mongoose = require('mongoose')

/**
 * serverId and channelId are static to identify where to look for the messages
 * messageIf is variable, and will be updated to the most recent message
 */
const messageSchema = new mongoose.Schema({
    guildId: {
        type: String,
        require: true
    },
    channelId: {
        type: String,
        required: true
    },
    messageId: {
        type: String,
        required: true
    },
    serverName: {
        type: String,
        required: true
    }
})

let messageModel
if (process.env.DEBUG === 'true') messageModel = mongoose.model('messageDEBUG', messageSchema)
else messageModel = mongoose.model('message', messageSchema)

module.exports = messageModel
