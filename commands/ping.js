module.exports = {
    name: 'ping',
    aliases: ['p', 'latency'],
    description: 'Zeigt den aktuellen Ping zum Bot',
    execute(message) {
        const delay = Date.now() - message.createdAt
        message.reply(`*(delay: ${delay}ms)*`)
    }
}
