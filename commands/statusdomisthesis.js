module.exports = {
    name: 'statusdomisthesis',
    new: true,
    hidden: true,
    disabled: false,
    inDevelopment: true,
    aliases: ['dt', 'domi'],
    description: 'Return random funny message about state of thesis',
    execute(message, args, bot) {
        let options = Math.floor(Math.random() * 2)

        if (options === 0) {
            let reaction = reactions[Math.floor(Math.random() * reactions.length)]
            message.react(reaction).catch(err => {
                console.log(err)
            })
        } else {
            let status = messages[Math.floor(Math.random() * messages.length)]
            message.reply(status)
        }
    }
}

const messages = [
    'Frag lieber nicht ...',
    'Kritisch?!',
    'Wird eng!',
    'Hunz elend',
    'Bestenfalls 35%',
]

const reactions = [
    "🙁",
    "😟",
    "😩",
    "💩",
    "🖕",
    "👀",
    "🥲"
]
