const translate = require('deepl')

module.exports = {
    name: 'translate',
    new: true,
    disabled: false,
    inDevelopment: true,
    aliases: ['t', 'trans'],
    options: ['DE', 'FR', 'IT', 'PL', 'NL', 'RU', 'RO'],
    usage: '<ziel-sprache> <text>',
    description: 'Übersetzt den `<text>` nach `<ziel-sprache>`',
    execute(message, args, bot) {
        let language = args[0].toUpperCase()
        if (precheck(language, args, message)) {
            // Remove command and Language
            let text = message.content.split(' ').slice(2).join(' ')
            translateText(language, text, message, bot)
        }
    }
}

function translateText(language, text, message, bot) {
    translate({
        free_api: true,
        text: text,
        target_lang: Languages[language],
        auth_key: process.env.DEEPL_TOKEN
    })
        .then(result => {
            message.reply(result.data.translations[0].text)
        })
        .catch(error => {
            handleDeeplError(Languages.DE, error, message, bot)
        })
}

function precheck(language, args, message) {
    if (!args[0]) {
        message.reply('Du musst eine Zielsprache wählen!\nGebe `!help` ein, um mögliche Sprachen zu listen!')
        return false
    }
    if (Languages[language] === undefined) {
        message.reply('Du musst eine gültige Zielsprache wählen!\nGebe `!help` ein, um mögliche Sprachen zu listen!')
        return false
    }
    if (!args[1]) {
        message.reply('Du musst etwas eingeben um übersetzen zu können :slight_smile:')
        return false
    }
    return true
}

function handleDeeplError(targetLang, error, message, bot) {
    bot.log.error(`StatusCode: "${error.statusCode}"`)
    let info
    switch (error.statusCode) {
        case 400: {
            info = `Ohoh.. Scheint als gäbe es ein Problem mit internen Parametern.\nWende dich bitte an den Bot-Admin ♥`
            break
        }
        case 403: {
            info = `Ohje.. Es scheint als wurde dem Bot den Zugriff verweigert :frowning:\nWende dich bitte an den Bot-Admin ♥`
            break
        }
        case 404: {
            info = `Die Resource konnte nicht gefunden werden. Versuche es erneut oder wende dich an den Bot-Admin :frowning:`
            break
        }
        case 456: {
            info = `Hoppla, es scheint als hätte der Bot alle Übersetzungen aufgebraucht.\nWende dich bitte an den Bot-Admin ♥`
            break
        }
        case 503: {
            info = `Tut mir leid, aber Übersetzungen sind im Moment nicht erreichbar :frowning:\nVersuche es später erneut.`
            break
        }
        case 429:
        case 529: {
            info = `Hoppla, es scheint als würde der Bot zu viele Anfragen bekommen\nWarte kurz und versuche es erneut ♥`
            break
        }
        case 413:
        case 414:
        default: {
            info = `Ups.. Es scheint ein internes Problem gegeben zu haben - Versuche es einfach erneut :slight_smile: `
            break
        }
    }
    bot.log.error(`${error.message}`)
    message.reply(info)
}

const Languages = {
    BG: 'BG',
    CS: 'CS',
    DA: 'DA',
    DE: 'DE',
    EL: 'EL',
    EN_GB: 'EN-GB',
    EN_US: 'EN-US',
    EN: 'EN',
    ES: 'ES',
    ET: 'ET',
    FI: 'FI',
    FR: 'FR',
    HU: 'HU',
    IT: 'IT',
    JA: 'JA',
    LT: 'LT',
    LV: 'LV',
    NL: 'NL',
    PL: 'PL',
    PT_PT: 'PT-PT',
    PT_BR: 'PT-BR',
    PT: 'PT',
    RO: 'RO',
    RU: 'RU',
    SK: 'SK',
    SL: 'SL',
    SV: 'SV',
    ZH: 'ZH'
}
