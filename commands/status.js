const plutoniumHelper = require('../helper/plutoniumHelper')
const messageModel = require('../db/schema')

module.exports = {
    name: 'status',
    aliases: ['s', 'mw3', 'plutonium', 'greba', 'pluto'],
    usage: '(<Servername>)', // Usage without command - represents the args only
    description: 'Prüft und zeigt den aktuellen Status des `<servername>` Plutonium MW3 Servers an.\nFalls `<servername>` nicht angegeben wird, wird standardmäßig nach einem Server mit dem Namen `Greba` geschaut',
    async execute(message, args, bot) {
        // fixme custom Server name disabled, as the Bot is EXCLUSIVELY used for our own `Greba`-Server
        // Custom Server name is ignored.
        // let serverName
        // if (args[0]) serverName = message.content.split(' ').slice(1).join(' ')
        // else serverName = defaultServer

        await showStatus(message, bot, defaultServer)
    }
}

const defaultServer = 'Greba'

async function showStatus(message, bot, serverName) {
    bot.log.info(`Sending Discord Status message!`)
    const time = new Date()
    const jsonStatus = await plutoniumHelper.fetchPlutoniumServerStatus(bot, serverName, defaultServer)
    const embed = plutoniumHelper.getEmbed(bot, time, jsonStatus)

    showEmbed(message, bot, serverName, embed)
}

function showEmbed(message, bot, serverName, embed) {
    const row = plutoniumHelper.getStartPlutoniumActionRow()
    message.channel.send({embeds: [embed], components: [row]})
        .then(sent => {
            bot.log.info(`Discord Status message sent, storing in DB(If not exists)!`)
            storeMessageToDb(sent, bot, serverName)
        })
}

/**
 * Stores message to the database.
 * Creates a new entry if it does not yet exist, or updates the messageId if an entry exists
 */
async function storeMessageToDb(message, bot, serverName) {
    try {
        const filter = {guildId: message.guild.id, channelId: message.channelId}
        const update = {
            guildId: message.guild.id,
            channelId: message.channel.id,
            messageId: message.id,
            serverName: serverName // Servername is 'Greba' -> The name that was initially searched for, NOT the actual hostname of a found server!
        }
        await messageModel.findOneAndUpdate(filter, update, {upsert: true})
    } catch (e) {
        bot.log.error(e)
    }
}
