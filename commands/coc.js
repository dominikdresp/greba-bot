const cocApi = require('clash-of-clans-api')
const discord = require("discord.js");

// 250 Requests/Month
const cocClient = cocApi({
    token: process.env.COC_TOKEN,
    request: {
        proxy: process.env.QUOTAGUARDSTATIC_URL
    }
})

// TODO Implement (Database needed)
// 500 Requests/Month
const cocAltClient = cocApi({
    token: process.env.COC_TOKEN,
    request: {
        proxy: process.env.FIXIE_URL
    }
})

module.exports = {
    name: 'clash',
    new: false,
    disabled: false,
    inDevelopment: false,
    aliases: ['coc', 'ci'],
    options: ['spieler', 'clan'],
    usage: '<option> <tag>',
    description: 'Zeigt Clash of Clans Infos zu `<option>` mit angegebenen `<tag>`',
    async execute(message, args, bot) {
        if (precheck(this.options, args, message)) {
            switch (args[0]) {
                case 'spieler': {
                    handleGetPlayerInfo(args[1], message, bot)
                    break
                }
                case 'clan': {
                    handleGetClanInfo(args[1], message, bot)
                    break
                }
            }
        }
    }
}

function precheck(options, args, message) {
    let availableOptions = '`'
    options.forEach(o => {
        availableOptions += `${o}, `
    })
    availableOptions = availableOptions.substring(0, availableOptions.length - 2) // Remove last comma and empty space
    availableOptions += '`'

    if (!args[0]) {
        let info = 'Du hast vergessen eine `<option>` anzugeben!\nEs gibt folgende Optionen zur Auswahl:' + `${availableOptions}`
        message.reply(info)
        return false
    }
    if (!options.find(o => o === args[0])) {
        let info = 'Du hast eine ungültige `<option>` angegeben!\nGültige Optionen sind:' + `${availableOptions}`
        message.reply(info)
        return false
    }
    if (!args[1]) {
        message.reply('Du hast vergessen einen `<tag>` zu deiner `<option>` anzugeben!')
        return false
    }
    return true
}

function handleGetPlayerInfo(tag, message, bot) {
    getPlayerInfo(tag)
        .then(jsonPlayerInfo => {
            printPlayerInfo(jsonPlayerInfo, message, bot)
        })
        .catch(err => {
            handleCoCError('Spieler', tag, err, message, bot)
        })
}

function handleGetClanInfo(tag, message, bot) {
    getClanInfo(tag)
        .then(jsonClanInfo => {
            printClanInfo(jsonClanInfo, message, bot)
        })
        .catch(err => {
            handleCoCError('Clan', tag, err, message, bot)
        })
}

// Fetch coc player info
async function getPlayerInfo(tag) {
    return await cocClient.playerByTag(tag)
}

// Fetch coc clan info
async function getClanInfo(tag) {
    return await cocClient.clanByTag(tag)
}

function toString(text) {
    return `${text}`
}

function printPlayerInfo(playerInfo, message, bot) {
    const embed = new discord.MessageEmbed()
        .setTitle("Clash of Clans Player Info")
        .setColor(bot.config.defaultColors.warning)
        .addField("Name", toString(playerInfo.name))
        .addField("Level", toString(playerInfo.expLevel))
        .addField("Rathaus Level", toString(playerInfo.townHallLevel))

    // In Case a player lost his trophies after a reset
    if (playerInfo.league)
        embed.addField("Liga", toString(playerInfo.league.name))

    embed
        .addField("Trophäen", toString(playerInfo.trophies))
        .addField("Clan", toString(playerInfo.clan.name))
        .addField("Rolle", toString(playerInfo.role))

    message.channel.send({embeds: [embed]});
}

function printClanInfo(clanInfo, message, bot) {
    console.log(JSON.stringify(clanInfo))
    const embed = new discord.MessageEmbed()
        .setTitle("Clash of Clans Clan Info")
        .setColor(bot.config.defaultColors.warning)
        .addField("Name", toString(clanInfo.name))
        .addField("Beschreibung", toString(clanInfo.description))
        .addField("Land", toString(clanInfo.location.name))
        .addField("Clan Level", toString(clanInfo.clanLevel))
        .addField("Clan Punkte", toString(clanInfo.clanPoints))
        .addField("Mitglieder", toString(clanInfo.members))
        .addField("Clan War Liga", toString(clanInfo.warLeague.name))
        .addField("Clan Krieg Siege", toString(clanInfo.warWins))
        .addField("Clan Krieg Niederlagen", toString(clanInfo.warLosses))

    message.channel.send({embeds: [embed]});
}

function handleCoCError(option, tag, error, message, bot) {
    bot.log.error(`StatusCode: "${error.statusCode}"`)
    let info
    switch (error.statusCode) {
        case 400: {
            info = `Ohoh.. Scheint als gäbe es ein Problem mit internen Parametern\nWende dich bitte an den Bot-Admin ♥`
            break
        }
        case 403: {
            info = `Ohje.. Es scheint als wurde dem Bot den Zugriff verweigert :frowning:\nWende dich bitte an den Bot-Admin ♥`
            break
        }
        case 404: {
            info = `${option} mit dem Tag: ${tag} konnte nicht gefunden werden und existiert nicht :frowning:`
            break
        }
        case 429: {
            info = `Hoppla, es scheint als würde der Bot zu viele Anfragen bekommen\nWende dich bitte an den Bot-Admin ♥`
            break
        }
        case 503: {
            info = `Tut mir leid, aber Clash of Clans ist momentan wegen Wartungsarbeiten nicht erreichbar :frowning:`
            break
        }
        case 500:
        default: {
            info = `Ups.. Es scheint ein unbekanntes Problem gegeben zu haben - Versuche es einfach erneut :slight_smile: `
            break
        }
    }
    bot.log.error(`${option} Error with Code: ${error.message}`)
    message.reply(info)
}
