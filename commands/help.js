const discord = require("discord.js");
const {prod_prefix, debug_prefix} = require("../config.json")
const commandPrefix = (process.env.DEBUG === 'true') ? debug_prefix : prod_prefix

module.exports = {
    name: 'help',
    aliases: ['h', 'info'],
    description: 'Listet alle verfügbaren Befehle',
    execute(message, args, bot) {
        const embed = new discord.MessageEmbed()
            .setTitle("Verfügbare Befehle")
            .setColor(bot.config.defaultColors.neutral)

        bot.commands.forEach(command => {
            if (command.disabled && command.disabled === true) return
            if (command.hidden && command.hidden === true) return
            if (command.name != null && command.description != null) {
                // Add Aliases
                let info = "__Alias:__ `"
                command.aliases.forEach(a => {
                    info += `${a}, `
                })
                info = info.substring(0, info.length - 2) // remove last needless comma
                info += '`\n'

                // Add command usage if exists
                if (command.usage) {
                    info += "__Nutzung:__ `" + `${commandPrefix}${command.name} ${command.usage}` + "`\n"
                }

                // Add command Options if exists
                if (command.options) {
                    info += "__Optionen:__ `"
                    command.options.forEach(o => {
                        info += `${o}, `
                    })
                    info = info.substring(0, info.length - 2) // remove last needless comma
                    info += "`\n"
                }

                // Add command Description
                info += `__Beschreibung:__ ${command.description}\n\n`
                let newCommandPrefix = command.new ? ':tada: ' : ''
                embed.addField(`${newCommandPrefix}${commandPrefix}${command.name}`, info)
            }
        })
        message.channel.send({embeds: [embed]});
    }
}
