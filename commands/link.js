const got = require('got')
const {link_shorten_url} = require('../config.json')
const discord = require("discord.js");

module.exports = {
    name: 'link',
    new: true,
    disabled: false,
    inDevelopment: false,
    aliases: ['l', 'li', 'short'],
    usage: '<url>',
    description: 'Kürzt und ersetzt die `<url>`',
    async execute(message, args, bot) {
        if (precheck(args, message)) {
            const options = buildRequestOptions(args[0])
            got(options)
                .then(response => {
                    message.delete()
                    printLinkInfo(response.body.shortURL, message)
                    console.log(response.body)
                })
                .catch(err => handleErrors(err))
        }
    }
}

function precheck(args, message) {
    if (args.length < 1) {
        message.reply(`Du musst eine URL angeben! :slight_smile:`)
        return false
    }
    if (args.length > 1) {
        message.reply(`Du kannst nur eine einzige URL angeben! :slight_smile:`)
        return false
    }
    return true
}

function buildRequestOptions(url) {
    return {
        method: 'POST',
        url: 'https://api.short.io/links',
        headers: {
            authorization: process.env.SHORT_IO_TOKEN,
        },
        json: {
            originalURL: url,
            domain: link_shorten_url
        },
        responseType: 'json'
    }
}

function printLinkInfo(shortUrl, message) {
    message.channel.send(`${message.author}\n${shortUrl}`)
}

// TODO handle response codes
function handleErrors(error) {
    console.log(error.message)
}
