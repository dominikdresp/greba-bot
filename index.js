require('dotenv').config()
// Keep Bot alive
require('./status_page')
// Require the necessary discord.js classes
const {Client, Intents, Collection} = require('discord.js')
const {prod_prefix, debug_prefix, name} = require('./config.json')
const fs = require('fs')

const mongoose = require('mongoose')
const plutoniumHelper = require("./helper/plutoniumHelper");

// region Setup depending on Debug mode
const commandPrefix = (process.env.DEBUG === 'true') ? debug_prefix : prod_prefix
const botToken = (process.env.DEBUG === 'true') ? process.env.DEV_TOKEN : process.env.TOKEN
const allowDevelopmentCmds = (process.env.DEBUG === 'true')
// endregion

// region Logging
const winston = require('winston')
const chalk = require('chalk')
const cron = require("node-cron");

// define log levels
const logLevels = {
    error: 0,
    warn: 1,
    info: 2,
    modules: 3,
    modwarn: 4,
    modinfo: 5,
    debug: 6,
}

// define log colours
winston.addColors({
    error: 'red',
    warn: 'yellow',
    info: 'green',
    modules: 'cyan',
    modwarn: 'yellow',
    modinfo: 'green',
    debug: 'blue',
})

const logger = winston.createLogger({
    levels: logLevels,
    transports: [new winston.transports.Console({colorize: true, timestamp: true})],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.padLevels({levels: logLevels}),
        winston.format.timestamp(),
        winston.format.printf(info => `${info.timestamp} ${info.level}:${info.message}`),
    ),
    level: 'debug',
})
// endregion

// Bot Config
const configSchema = {
    name,
    defaultColors: {
        success: '#70e000',
        neutral: '#17c3b2',
        warning: '#fcbf49',
        error: '#e63946'
    }
}

// Create a new bot client instance
const bot = {
    client: new Client({intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES]}),
    log: logger,
    commands: new Collection(),
    config: configSchema
}

bot.load = function load() {
    this.log.info('Loading commands...')
    const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js') || file.endsWith('.mjs'))
    for (const file of commandFiles) {
        const cmd = require(`./commands/${file}`)
        this.commands.set(cmd.name, cmd)
    }

    this.log.info('Connecting...')
    let message = (process.env.DEBUG === 'true') ? 'DEBUG MODE' : 'PRODUCTION MODE'
    this.log.info(chalk.yellow(message))
    this.client.login(botToken)
}

bot.onConnect = async function onConnect() {
    const options = {
        keepAlive: true,
        keepAliveInitialDelay: 30000
    }
    try {
        await mongoose.connect(process.env.MONGODB_URI, options).then(
            () => {
                bot.log.warn(`Connected to MongoDB`)
            },
            err => {
                bot.log.warn(`Connection to MongoDB failed: ${err}`)
            }
        )
    } catch (e) {
        bot.log.error(e)
    }

    // Initiates cron-job to update plutonium server status everywhere
    scheduleUpdate()
    schedulePresenceUpdate()

    this.log.info(chalk.green(`Bot ${this.client.user.tag} ist Ready!`))
}

// region tasks
let updateTask = undefined
let presenceTask = undefined

function scheduleUpdate() {
    if (updateTask !== undefined) return
    // second(optional) | minute | hour | day of month | month | day of week
    //  */10 => Every 10 seconds
    updateTask = cron.schedule('* * * * *', () => {
        bot.log.info(`Running Plutonium Server Status check (every 1 Minute)`)
        plutoniumHelper.updateServerStatus(bot)
    }, [])
}

function schedulePresenceUpdate() {
    if (presenceTask !== undefined) return
    presenceTask = cron.schedule('* * * * *', () => {
        bot.log.info(`Updating Bot Presence (every 1 Minute)`)
        updatePresence()
    }, [])
}

async function updatePresence() {
    const today = new Date()

    const messageSelectionFlag = (today.getMinutes() % 2) === 0

    let message
    let type
    if (false) {
        message = (process.env.DEBUG === 'true') ? `${debug_prefix}${bot.commands.get('help').name}`
            : `${prod_prefix}${bot.commands.get('help').name}`
        type = 'LISTENING'
    } else {
        const playerCount = await plutoniumHelper.fetchPlutoniumServerPlayerCount(bot)
        message = `mit ${playerCount.inServer}/${playerCount.playersOnline} Spielern`
        type = 'PLAYING'
    }

    bot.client.user.setPresence({
        status: "online",
        afk: true,
        activities: [
            {
                name: message,
                type: type
            }
        ]
    })
}

// endregion

bot.onMessageCreate = async function onMessageCreate(message) {
    // Ignore all messages without prefix
    if (!message.content.startsWith(commandPrefix)) return

    const args = message.content.split(/ +/)
    const enteredCommand = args
        .shift() // remove all args except command
        .toLowerCase()
        .slice(commandPrefix.length) // remove prefix

    // Get bot command, allowing for aliases
    const command = this.commands.get(enteredCommand) || this.commands.find(c => c.aliases && c.aliases.includes(enteredCommand))
    // Ignore if command does not exist
    if (!command) return
    // Ignore if command got disabled. Allow in-development commands if in Debug mode
    if (command.disabled && command.disabled === true) {
        this.log.warn(`Entered disabled command: ${enteredCommand}`)
        if (command.inDevelopment && command.inDevelopment === true)
            message.reply(`Dieser Befehl befindet sich derzeit noch in der Entwicklung.\nBald ist es soweit! :tada:`)
        return
    }

    try {
        command.execute(message, args, bot)
    } catch (error) {
        this.log.error(chalk.red(error))
        message.reply('Ups.. Es scheint als hätte es beim Ausführen des Befehls ein Problem gegeben :(!')
    }

    this.log.info(message.content)
}

// Register Event listeners
bot.client.on('ready', bot.onConnect.bind(bot))
bot.client.on('error', err => {
    bot.log.error(chalk.red(`Client error: ${err.message}`))
})
bot.client.on('messageCreate', bot.onMessageCreate.bind(bot))

// Start the Bot
bot.load()
