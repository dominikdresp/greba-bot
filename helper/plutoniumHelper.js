const messageModel = require("../db/schema");
const fetch = require("node-fetch");
const settings = {method: "Get"};
const {plutonium_url} = require("../api.json");
const discord = require("discord.js");
const {MessageActionRow, MessageButton} = require("discord.js");

module.exports = {
    fetchPlutoniumServerStatus,
    fetchPlutoniumServerPlayerCount,
    updateServerStatus,
    getStartPlutoniumActionRow,
    getEmbed
}

function getEmbed(bot, time, jsonStatus) {
    if (jsonStatus.server === undefined) {
        return getErrorEmbed(bot, time)
    } else {
        return getServerStatusEmbed(bot, jsonStatus, time)
    }
}

function getServerStatusEmbed(bot, jsonStatus, time) {
    const dateTime = time.toLocaleString('de-DE', {timeZone: "Europe/Berlin"})
    const formattedTime = dateTime.substring(0, dateTime.length - 3)
    let embed = new discord.MessageEmbed()
        .setTitle("__Plutonium Server Status__")
        .setDescription(`[Admin Console](https://admin.greba.one/)`)
        .setThumbnail('https://greba.one/content/plutonium/images/plutonium_logo.png')
        .setColor(bot.config.defaultColors.success)
        .addField("*Letzte Aktualisierung*", "```" + `${formattedTime} Uhr` + "```")
        .addField("**Servername**", cleanHostname(jsonStatus.server.hostname))
        .addField("**Server Öffentlich**", getIsPublic(jsonStatus.server.password))
        .addField("Spiel", getGameName(jsonStatus.server.game))
        .addField("Map", getMapName(jsonStatus.server.map))
        .addField("Modus", getGameType(jsonStatus.server.gametype))
        .addField("Spieler Online", `${jsonStatus.server.players.length}`)

    // TODO maybe extract to function which only returns finished formatted string
    // TODO also extract fetching! this function should only create the embed, not fetch anything
    let string = ""
    jsonStatus.server.players.forEach(player => {
        const name = player.username
        const ping = player.ping
        const iw4mClient = jsonStatus.clients.find(c => c.name === name)
        const clientId = iw4mClient.clientId
        const profileUrl = `${process.env.IW4MADMIN_URL}/client/${clientId}`
        string += `\n[${name}](${profileUrl}) | ${ping}ms`
    })

    // Only add Players when at least 1 Players is in the Server
    if (string.length !== 0) embed.addField("Spieler | Ping", string)
    return embed
}

const profileIcon = '🔗'

// Remove color coding characters from Servername
function cleanHostname(hostname) {
    return hostname.replaceAll(/\^\d/g, "")
}

function getIsPublic(hasPassword) {
    switch (hasPassword) {
        case true:
            return "❌"
        case false:
            return "✅"
        default:
            return "❔"
    }
}

function getErrorEmbed(bot, time) {
    return new discord.MessageEmbed()
        .setTitle("Server Offline")
        .addField("*Letzte Aktualisierung*", "```" + `${time.toLocaleString('de-DE', {timeZone: "Europe/Berlin"})}` + "```")
        .setColor(bot.config.defaultColors.error)
}

async function fetchPlutoniumServerPlayerCount(bot) {
    bot.log.info(`Fetching Plutonium `)
    const response = await fetch(plutonium_url, settings)
    const json = await response.json()
    const playersOnline = Object.values(json)
        .filter(s => s.game === 'iw5mp') // Take only MW3 servers into account
        .map(s => s.players.length)
        .reduce((accumulator, a) => {
            return accumulator + a
        }, 0)

    let inServer = Object.values(json)
        .filter(s =>
            s.hostname.toLowerCase().includes('greba')
        )
        .map(s => s.players.length)
        .reduce((accumulator, a) => {
            return accumulator + a
        }, 0)

    return {
        "playersOnline": playersOnline,
        "inServer": inServer
    }
}

// TODO Maybe cache results with timestamp to reuse for all other servers, using the same hostname
// Else we will fetch 5 times, when having 5 channels asking for an update :(
/**
 * Fetches JSON from Plutonium API
 * @return The highest priority Server (Player-Count, port-number as fallback)
 */
async function fetchPlutoniumServerStatus(bot, serverName, defaultServer) {
    bot.log.info(`Fetching Plutonium server status for server: ${serverName}`)
    const response = await fetch(plutonium_url, settings)
    const json = await response.json()
    const servers = Object.values(json).filter(s =>
        s.hostname.toLowerCase().includes(serverName.toLowerCase())
    ).sort( // Sort by port number
        (a, b) => b.port - a.port
    ).sort( // Sort by player count
        (a, b) => b.players.length - a.players.length
    )

    // Take the first (sorted by priority) server with clients
    let server = undefined
    let clients = undefined
    if (servers[0]) {
        server = servers[0]
        clients = await fetchIw4mClients(bot, server.players)
    }

    return {
        "server": server,
        "clients": clients
    }
}

async function fetchIw4mClients(bot, players) {
    let clients = []
    for (const player of players) {
        await fetch(`${process.env.IW4MADMIN_URL}/api/client/find/?name=${player.username}`)
            .then(res => res.json())
            .then(json => {
                bot.log.error(`Mapped player Client: ${JSON.stringify(json.clients[0])}`)
                clients.push(json.clients[0])
            })
    }
    return clients
}

async function updateServerStatus(bot) {
    await messageModel.find({}, (err, docs) => {
        if (err) bot.log.error(err)
        bot.log.info(`Updating server status for ${docs.length} Servers.`)
        docs.forEach(async (entry) => {
            updateMessage(bot, entry, entry.serverName)
        })
    }).clone().catch(function (err) {
        console.log(err)
    })
}

async function updateMessage(bot, document, serverName) {
    const time = new Date()
    const jsonStatus = await fetchPlutoniumServerStatus(bot, serverName)
    const embed = getEmbed(bot, time, jsonStatus)
    const row = getStartPlutoniumActionRow()

    bot.client.channels.cache
        .get(document.channelId)
        .messages
        .fetch(document.messageId)
        .then(msg => {
            msg.edit({embeds: [embed], components: [row]})
                .catch(err => {
                    bot.log.error(`Editing Message failed:\n${err}`)
                })
        })
        .catch(err => {
            bot.log.error(`${err}\nMessage with ID: ${document.messageId} has probably been deleted. Removing Database entry.`)
            removeDbEntry(document)
        })
}

async function removeDbEntry(document) {
    await messageModel.deleteOne(document)
}

function getStartPlutoniumActionRow() {
    return new MessageActionRow()
        .addComponents(
            new MessageButton()
                .setURL(`https://start.greba.one/iw5`)
                .setLabel('Plutonium Starten')
                .setStyle('LINK'),
        )
}

function getGameName(game) {
    switch (game) {
        case 'iw5mp':
            return 'Modern Warfare 3'
        case 't6zm':
            return 'Black Ops 2: Zombies'
        case 't6mp':
            return 'Black Ops 2: Multiplayer'
        case 't4mp':
            return 'World at War'
        case 't4sp':
            return 'World at War: Zombies'
        default:
            return game
    }
}

function getGameType(type) {
    switch (type) {
        case 'war' || 'tdm':
            return 'Team Deathmatch'
        case 'dm':
            return 'Frei für Alle'
        case 'conf':
            return 'conf'
        case 'sd':
            return 'Suchen & Zerstören'
        case 'ctf':
            return 'Capture the Flag'
        case 'dom':
            return 'Herrschaft'
        case 'gun':
            return 'Waffenspiel'
        case 'infect':
            return 'Infiziert'
        case 'jugg':
            return 'jugg'
        case 'dem':
            return 'Demolition' // TODO translate
        case 'koth':
            return 'Hauptquartier'
        case 'oic':
            return 'Eine im Lauf'
        case 'sab':
            return 'Sabotage'
        default:
            return type
    }
}

function getMapName(map) {
    switch (map) {
        case 'mp_alpha':
            return "Lockdown"
        case 'mp_bootleg':
            return "Bootleg"
        case 'mp_bravo':
            return "Mission"
        case 'mp_carbon':
            return "Carbon"
        case 'mp_dome':
            return "Dome"
        case 'mp_exchange':
            return "Downturn"
        case 'mp_hardhat':
            return "Hardhat"
        case 'mp_interchange':
            return "Interchange"
        case 'mp_lambeth':
            return "Fallen"
        case 'mp_mogadishu':
            return "Bakaara"
        case 'mp_paris':
            return "Resistance"
        case 'mp_plaza2':
            return "Arkaden"
        case 'mp_radar':
            return "Outpost"
        case 'mp_seatown':
            return "Seatown"
        case 'mp_underground':
            return "Underground"
        case 'mp_village':
            return "Village"
        case 'mp_terminal_cls':
            return "Terminal"
        case 'mp_aground_ss':
            return "Aground"
        case 'mp_courtyard_ss':
            return "Erosion"
        case 'mp_italy':
            return "Piazza"
        case 'mp_overwatch':
            return "Overwatch"
        case 'mp_morningwood':
            return "Black Box"
        case 'mp_park':
            return "Liberation"
        case 'mp_meteora':
            return "Sanctuary"
        case 'mp_cement':
            return "Foundation"
        case 'mp_qadeem':
            return "Oasis"
        case 'mp_restrepo_ss':
            return "Lookout"
        case 'mp_hillside_ss':
            return "Getaway"
        case 'mp_crosswalk_ss':
            return "Intersection"
        case 'mp_burn_ss':
            return "U-Turn"
        case 'mp_six_ss':
            return "Vortex"
        case 'mp_boardwalk':
            return "Boardwalk"
        case 'mp_moab':
            return "Gulch"
        case 'mp_roughneck':
            return "Off Shore"
        case 'mp_shipbreaker':
            return "Decommission"
        case 'mp_nola':
            return "Parish"
        case 'mp_rust':
            return "Rust"
        case 'mp_highrise':
            return "Highrise"
        case 'mp_nuked':
            return "Nuketown"
        case 'mp_nightshift':
            return "Skidrow"
        case 'mp_favela':
            return "Favela"
        case 'mp_shipment':
            return "Shipment"
        case 'mp_killhouse':
            return "Killhouse"
        case 'mp_rust_long':
            return "Rust Long"
        case 'mp_shipmentlong':
            return "Shipment Long"
        case 'mp_bog':
            return "Bog"
        case 'mp_bo2_town':
            return "Town"
        case 'mp_bo2cove':
            return "Cove"
        case 'mp_bo2frost':
            return "Frost"
        case 'mp_bo2grind':
            return "Grind"
        case 'mp_bo2paintball':
            return "Paintball"
        case 'mp_bo2raid3':
            return "Raid"
        case 'mp_boneyard':
            return "Boneyard"
        case 'mp_broadcast':
            return "Broadcast"
        case 'mp_countdown':
            return "Countdown"
        case 'mp_crash':
            return "Crash"
        case 'mp_crossfire':
            return "Crossfire"
        case 'mp_osg_hijacked_2':
            return "Hijacked"
        case 'mp_osg_standoff':
            return "Standoff"
        case 'mp_overgrown':
            return "Overgrown"
        case 'mp_overpass':
            return "CSGO: Overpass"
        case 'mp_shortdust':
            return "CSGO: Shortdust"
        case 'mp_summit':
            return "Summit"
        default:
            return map
    }
}
